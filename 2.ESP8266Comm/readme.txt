Q1.Ways in which you can connect the NodeMcu ESP wifi module to the Internet and send data remotely without using Port forwarding (ie no changes to be made in the router settings).
The data should be sent securely to the ESP module

One possible Solution:
-By connecting both ESP8266 to IBM Platform & establishing MQTT connectivity, one ESP8266 can be used to publish messages on a specific topic while the other ESP8266 can receive those messages by subscriving to the same topic.
-This solution involves the use of IBM Watson IOT platform & lua files modified accordingly
-This solution will work irrespective of the network to which the 2 ESP8266 are connected



