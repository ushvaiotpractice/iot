a=spi.setup(1, spi.MASTER, spi.CPOL_LOW, spi.CPHA_LOW, 8, 8)
print(a)
server_ip=nil
timestamp=nil
i=0
-- initialize other spi slaves

-- then mount the sd
-- note: the card initialization process during `file.mount()` will set spi divider temporarily to 200 (400 kHz)
-- it's reverted back to the current user setting before `file.mount()` finishes
vol = file.mount("/SD0", 8)   -- 2nd parameter is optional for non-standard SS/CS pin

if not vol then
  print("retry mounting")
  while(i<6) do 
  vol = file.mount("/SD0", 8)
  print("vol")
  print(vol)
  i=i+1
  if vol then
  break
  end
  end 
  if not vol then
    print("mount failed")
  end
end

files=file.list()
print(string.format("List of files: %s",files))

if file.exists("tempfile.txt")
then
print("File exists")
else
print("file does not exist")
end

if file.open("/SD0/tempfile.txt","w")
then

sntp.sync(server_ip,
  function()
    print("sntp time sync ok")
    file.on("rtc",
      function()
        timestamp= rtctime.epoch2cal(rtctime.get())
		print(string.format("Fetched Timestamp:%s",timestamp))
      end)
  end)
  
file.write(timestamp)
end 


file.open("/SD0/tempfile.txt")
print(file.read())
file.close()