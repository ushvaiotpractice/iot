Q1.Data is stored in bluemix server as a collection of JSON strings 

Publish interval (time elapsed between successive JSON publish attempts) is 10 seconds and so is the time interval between transmission of two JSON strings from TM4C to NodeMCU.
In absence of connectivity, data is stored in sd card. 
Whenever ESP gets connected to internet, data stored in UART buffer gets published followed by data from sd card. Provide   one possible algorithm to perform this operation.
Each JSON string must be timestamped  prior to the publish attempt. Suggest how this task can be performed with and without RTC.


Solution:
-mqttclient_ibm.lua module contains connection of ESP8266 using MQTT to IBM Watson IOT cloud
-Firmware requires custom build with modules like file,spi,rtctime,sntp,mqtt
-IBM Watson Trial account used: Username: vaish2619s@gmail.com , Password: Bluemix@123
-Absence of connectivity is verified inside init.lua module
-sdcard.lua module contains code for storing of data into SD card interfaced with ESP8266 
-Micro SD card adapter to ESP 8266 connections 
MISO (SD adapter)- MISO(Node MCU)
MOSI (SD adapter)- MOSI(Node MCU)
CLK (SD adapter)- HCLK(Node MCU)
Vcc (SD adapter)- Vin(Node MCU)
GND (SD adapter)- GND(Node MCU)
-Algorithm for saving data to SD card in the absence of connectivity:
a. Check if connectivity is available to IBM Cloud 
b.If Yes, use MQTT Client to publish messages to IBM Cloud
c.Format the data to be sent as JSON along with timestamp by obtaining timestamp using either RTC (& syncing with NTP server) or by fetching the time from NIST server
c. If No, open SPI connection to SD card & write data to it



