org="f87x38"
username="use-token-auth"
password="NodeBluemix@123"
publish_topic = "iot-2/evt/devicestatus/fmt/json"
server="f87x38.messaging.internetofthings.ibmcloud.com"
client_id="d:f87x38:nodemcu:10702602"
publish_data= "{ \"d\": { \"temp\": 25.0, \"led\": 0 } }"

m = mqtt.Client(client_id, 360, username, password )

mytimer = tmr.create()

wifi_disconnect_event = function(T)
  if T.reason == wifi.eventmon.reason.ASSOC_LEAVE then 
    --the station has disassociated from a previously connected AP
	print("Dissassociated from previous AP")
    return 
  end
  -- total_tries: how many times the station will attempt to connect to the AP. Should consider AP reboot duration.
  --local total_tries = 75
  local total_tries=1e309
  print("\nWiFi connection to AP("..T.SSID..") has failed!")

  --There are many possible disconnect reasons, the following iterates through 
  --the list and returns the string corresponding to the disconnect reason.
  debug_pin=1--GPIO5
  for key,val in pairs(wifi.eventmon.reason) do
    if val == T.reason then
      print("Disconnect reason: "..val.."("..key..")")
	  gpio.write(debug_pin,gpio.HIGH)
      break
    end
  end

  if disconnect_ct == nil then 
    disconnect_ct = 1 
  else
    disconnect_ct = disconnect_ct + 1 
  end
  if disconnect_ct < total_tries then 
    print("Retrying connection...(attempt "..(disconnect_ct+1).." of "..total_tries..")")
  else
    wifi.sta.disconnect()
    print("Aborting connection to AP!")
    disconnect_ct = nil  
  end
end


        	
m:on("connect", function(client) 

mytimer:register(10000, tmr.ALARM_AUTO, function() 
	m:publish(publish_topic, publish_data, 1, 1, function(client) print("published") end)
	print("timer running") end)
	if mytimer:start() then print("timer started") end 

print ("connected") end)

m:on("offline", function(client) print ("offline") tmr.create():alarm(10 * 1000, tmr.ALARM_SINGLE, do_mqtt_connect)  end)

m:on("message", function(client, topic, data) print(topic .. ":") end)


function do_mqtt_connect()
m:connect(server, 1883, 0, function(client)

  print("connected")
  -- Calling subscribe/publish only makes sense once the connection
  -- was successfully established. You can do that either here in the
  -- 'connect' callback or you need to otherwise make sure the
  -- connection was established (e.g. tracking connection status or in
    mytimer:register(10000, tmr.ALARM_AUTO, function() 
	m:publish(publish_topic, publish_data, 1, 1, function(client) print("published") end)
	print("timer running") end)
	if mytimer:start() then print("timer started") end 

  
  wifi.eventmon.register(wifi.eventmon.STA_DISCONNECTED, wifi_disconnect_event)
  

end,
function(client, reason)
  print("failed reason: " .. reason)
  handle_mqtt_error()
  wifi.eventmon.register(wifi.eventmon.STA_DISCONNECTED, wifi_disconnect_event)
  if not mytimer:stop() then print("timer not stopped, not registered?") end
  --gpio.write(pin,gpio.HIGH)
end)
end 

m:connect(server, 1883, 1, function(client)

  
  -- Calling subscribe/publish only makes sense once the connection
  -- was successfully established. You can do that either here in the
  -- 'connect' callback or you need to otherwise make sure the
  -- connection was established (e.g. tracking connection status or in
  mytimer:register(10000, tmr.ALARM_AUTO, function() 
	m:publish(publish_topic, publish_data, 1, 1, function(client) print("published") end)
	print("timer running") end)
	if mytimer:start() then print("timer started") end 

print ("connected")
  
  wifi.eventmon.register(wifi.eventmon.STA_DISCONNECTED, wifi_disconnect_event)
  

end,
function(client, reason)
  print("failed reason: " .. reason)
  handle_mqtt_error()
  wifi.eventmon.register(wifi.eventmon.STA_DISCONNECTED, wifi_disconnect_event)
  if not mytimer:stop() then print("timer not stopped, not registered?") end
  --gpio.write(pin,gpio.HIGH)
end)

function handle_mqtt_error(client, reason) 
  tmr.create():alarm(10 * 1000, tmr.ALARM_SINGLE, do_mqtt_connect)
end


do_mqtt_connect()
m:close();




-- you can call m:connect again