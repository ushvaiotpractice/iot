package com.trial.websocketexampleapplication;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.JsonObject;
import com.ibm.iotf.client.api.APIClient;
import com.ibm.iotf.client.app.ApplicationClient;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.util.Properties;

public class BluemixMessagingActivity extends AppCompatActivity {

    ApplicationClient applicationClient;
    APIClient apiClient;
    Properties options;
    Properties api_options;
    private static final String orgId="f87x38";
    private static final String id="app" + (Math.random() * 10000);
    private static final String authMethod="apikey";
    private static final String apiKey="a-f87x38-gvhgxflga7";
    private static final String authToken="&Z7vx5pKVeh!j0DI?9";
    private static final String deviceType="nodemcu";
    private static final String deviceId="10702602";

    Button buttonConnect;
    Button buttonCommand;
    Button buttonSendHttp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluemix_messaging);

        try {
            options=new Properties();
            options.put("org", orgId);
            options.put("id",id );
            options.put("Authentication-Method", authMethod);
            options.put("API-Key", apiKey);
            options.put("Authentication-Token", authToken);

            applicationClient = new ApplicationClient(options);


            api_options=new Properties();
            api_options.put("org", orgId);
            api_options.put("id",id );
            api_options.put("Authentication-Method", authMethod);
            api_options.put("API-Key", apiKey);
            api_options.put("Authentication-Token", authToken);
            api_options.put("Device-ID",deviceId);
            api_options.put("Device-Type",deviceType);

            apiClient=new APIClient(api_options);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        buttonConnect= (Button) findViewById(R.id.buttonConnect);

        buttonConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    new AppConnection().execute();


                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        });

        buttonCommand= (Button) findViewById(R.id.buttonPublish);
        buttonCommand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject data = new JSONObject();
                    data.put("name", "stop-rotation");
                    data.put("delay", 0);

                    new CommandPublish().execute(data);



                }
                catch (JSONException json_exp)
                {
                    json_exp.printStackTrace();
                }
            }
        });

        //buttonSendHttp= (Button) findViewById(R.id.buttonSendHttp);
        /*buttonSendHttp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JsonObject event = new JsonObject();
                    event.addProperty("reboot", 5);

                    boolean response = apiClient.publishCommandOverHTTP("execute", event);
                    if(response==true)
                    {
                        Toast.makeText(BluemixMessagingActivity.this,"Command published succesfully over Http!",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(BluemixMessagingActivity.this,"Error in publishing command!",Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception exp)
                {
                    exp.printStackTrace();
                }
            }
        });*/
    }

    private class AppConnection extends AsyncTask<Object,Boolean,Boolean>
    {

        @Override
        protected Boolean doInBackground(Object[] params) {
            try {
                applicationClient.setKeepAliveInterval(120);
                applicationClient.connect();
                return true;
            }
                catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }


        }

        @Override
        protected void onPostExecute(Boolean o) {
            if(o)
            {
                Toast.makeText(BluemixMessagingActivity.this,"Successfully connected to the IBM Watson IoT Platform!",Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(BluemixMessagingActivity.this,"Error in connecting to the IBM Watson IoT Platform!",Toast.LENGTH_SHORT).show();
            }
        }
    }


    private class CommandPublish extends AsyncTask<JSONObject,Boolean,Boolean>
    {


        @Override
        protected Boolean doInBackground(JSONObject... params) {
            try {
                applicationClient.publishCommand(deviceType, deviceId, "stop", params);
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean o) {
            if(o)
            {
                Toast.makeText(BluemixMessagingActivity.this,"Command published succesfully!",Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(BluemixMessagingActivity.this,"Error in publishing command to IBM Wtason IOT platform!",Toast.LENGTH_SHORT).show();
            }
        }
    }


}
