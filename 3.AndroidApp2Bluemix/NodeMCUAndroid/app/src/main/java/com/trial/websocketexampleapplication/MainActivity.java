package com.trial.websocketexampleapplication;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;

public class MainActivity extends AppCompatActivity {

    private  WebSocketClient webSocketClient;
    private static final String TAG="MainActivity";
    Button buttonsend;
    TextView textViewreceivedmsg;
    Button buttonnext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       buttonsend = (Button) findViewById(R.id.buttonSend);

        buttonsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                establishWebSocketconn();
            }
        });

        buttonnext= (Button) findViewById(R.id.buttonNext);

        textViewreceivedmsg= (TextView) findViewById(R.id.textViewreceivedmsg);


        buttonnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,BluemixMessagingActivity.class));
            }
        });
    }

    private void establishWebSocketconn()
    {
        URI uri=null;

        try
        {
            EditText editTextUrl= (EditText) findViewById(R.id.editTextUrl);
            String url=editTextUrl.getText().toString();
            if(!url.equals("")) {
                uri = new URI(String.format("ws://{0}:8080", url));
            }
            else
            {
                Toast.makeText(MainActivity.this,"Enter a valid url",Toast.LENGTH_SHORT).show();
            }
        }
        catch (URISyntaxException e)
        {
            e.printStackTrace();
            return;
        }


        webSocketClient=new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake handshakedata) {
                Log.i(TAG,"Websocket Connection opened");
                webSocketClient.send("Hello");
            }

            @Override
            public void onMessage(String message) {
                final String received_msg=message;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    textViewreceivedmsg.setText(received_msg);
                    }
                });

            }

            @Override
            public void onClose(int code, String reason, boolean remote) {
            Log.i(TAG,"Websocket connection closed");
            }

            @Override
            public void onError(Exception ex) {
                Log.i(TAG,"Error in Websocket connection"+ex.getMessage());
            }
        };
        webSocketClient.connect();
    }

    public void sendMessage()
    {
        EditText editTextsendmsg= (EditText) findViewById(R.id.editTextmsgsend);
        String msg_to_Send=editTextsendmsg.getText().toString();

        webSocketClient.send(msg_to_Send);
        Toast.makeText(MainActivity.this,"Sending message",Toast.LENGTH_SHORT).show();
    }
}
