Q3.How to send control command from Mobile App to IBM Bluemix ?
Provide all possible method. 

One possible Solution:
-By using  HTTP Messaging APIs(using MQTT & HTTP) to establish connection with IBM Bluemix and publish messages in a specific format.
-This will require Android application to be registered with IBM Bluemix to obtain API key and Authentaication token
-Implementation files:
	-BluemixMessagingActivity:2 buttons implemented: one for connecting to IBM Bluemix & the other to send publish /control commands
	-activity_bluemix_messaging: UI file for the above activity
	-All network operations are performed using Asynctask instead of main thread

